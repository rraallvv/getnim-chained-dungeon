/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Shattered Pixel Dungeon
 * Copyright (C) 2014-2019 Evan Debenham
 *
 * Chained Dungeon
 * Copyright (C) 2019 Rhody Lugo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.teknologika.chaineddungeon.scenes;

import com.teknologika.chaineddungeon.SPDSettings;
import com.teknologika.chaineddungeon.ChainedDungeon;
import com.teknologika.chaineddungeon.effects.Flare;
import com.teknologika.chaineddungeon.ui.Archs;
import com.teknologika.chaineddungeon.ui.ExitButton;
import com.teknologika.chaineddungeon.ui.Icons;
import com.teknologika.chaineddungeon.ui.RenderedTextMultiline;
import com.teknologika.chaineddungeon.ui.Window;
import com.watabou.input.Touchscreen.Touch;
import com.watabou.noosa.Camera;
import com.watabou.noosa.Image;
import com.watabou.noosa.RenderedText;
import com.watabou.noosa.TouchArea;
import com.watabou.utils.DeviceCompat;

public class AboutScene extends PixelScene {

	private static final String TTL_CHPX = "Chained Dungeon";

	private static final String TXT_CHPX = "GetNIM Integration, Code & Graphics: rraallvv";

	private static final String LNK_CHPX = "GetNIMapp.com";

	private static final String TTL_SHPX = "Shattered Pixel Dungeon";

	private static final String TXT_SHPX = "Design, Code, & Graphics: Evan";

	private static final String LNK_SHPX = "ShatteredPixel.com";

	private static final String TTL_WATA = "Pixel Dungeon";

	private static final String TXT_WATA = "Code & Graphics: Watabou\nMusic: Cube_Code";
	
	private static final String LNK_WATA = "pixeldungeon.watabou.ru";
	
	@Override
	public void create() {
		super.create();

		final float colWidth = Camera.main.width / (SPDSettings.landscape() ? 3 : 1);
		final float colTop = (Camera.main.height / 3) - (SPDSettings.landscape() ? 30 : 70);
		final float shpxOffset = SPDSettings.landscape() ? colWidth : 0;
		final float wataOffset = SPDSettings.landscape() ? 2 * colWidth : 0;

		Image chpx = Icons.CHPX.get();
		chpx.x = (colWidth - chpx.width()) / 2;
		chpx.y = colTop;
		align(chpx);
		add( chpx );

		new Flare( 7, 64 ).color( 0x654A09, true ).show( chpx, 0 ).angularSpeed = +20;

		RenderedText chpxtitle = renderText( TTL_CHPX, 8 );
		chpxtitle.hardlight( Window.CHPX_COLOR );
		add( chpxtitle );

		chpxtitle.x = (colWidth - chpxtitle.width()) / 2;
		chpxtitle.y = chpx.y + chpx.height + 5;
		align(chpxtitle);

		RenderedTextMultiline chpxtext = renderMultiline( TXT_CHPX, 8 );
		chpxtext.maxWidth((int)Math.min(colWidth, 120));
		add( chpxtext );

		chpxtext.setPos((colWidth - chpxtext.width()) / 2, chpxtitle.y + chpxtitle.height() + 12);
		align(chpxtext);

		RenderedTextMultiline chpxlink = renderMultiline( LNK_CHPX, 8 );
		chpxlink.maxWidth(chpxtext.maxWidth());
		chpxlink.hardlight( Window.CHPX_COLOR );
		add( chpxlink );

		chpxlink.setPos((colWidth - chpxlink.width()) / 2, chpxtext.bottom() + 6);
		align(chpxlink);

		TouchArea chpxhotArea = new TouchArea( chpxlink.left(), chpxlink.top(), chpxlink.width(), chpxlink.height() ) {
			@Override
			protected void onClick( Touch touch ) {
				DeviceCompat.openURI( "https://" + LNK_CHPX );
			}
		};
		add( chpxhotArea );

		Image shpx = Icons.SHPX.get();
		shpx.x = shpxOffset + (colWidth - shpx.width()) / 2;
		shpx.y = SPDSettings.landscape() ?
				colTop:
				chpxlink.top() + shpx.height + 20;
		align(shpx);
		add( shpx );

		new Flare( 7, 64 ).color( 0x225511, true ).show( shpx, 0 ).angularSpeed = +20;

		RenderedText shpxtitle = renderText( TTL_SHPX, 8 );
		shpxtitle.hardlight( Window.SHPX_COLOR );
		add( shpxtitle );

		shpxtitle.x = shpxOffset + (colWidth - shpxtitle.width()) / 2;
		shpxtitle.y = shpx.y + shpx.height + 5;
		align(shpxtitle);

		RenderedTextMultiline shpxtext = renderMultiline( TXT_SHPX, 8 );
		shpxtext.maxWidth((int)Math.min(colWidth, 120));
		add( shpxtext );

		shpxtext.setPos(shpxOffset + (colWidth - shpxtext.width()) / 2, shpxtitle.y + shpxtitle.height() + 12);
		align(shpxtext);

		RenderedTextMultiline shpxlink = renderMultiline( LNK_SHPX, 8 );
		shpxlink.maxWidth(shpxtext.maxWidth());
		shpxlink.hardlight( Window.SHPX_COLOR );
		add( shpxlink );

		shpxlink.setPos(shpxOffset + (colWidth - shpxlink.width()) / 2, shpxtext.bottom() + 6);
		align(shpxlink);

		TouchArea shpxhotArea = new TouchArea( shpxlink.left(), shpxlink.top(), shpxlink.width(), shpxlink.height() ) {
			@Override
			protected void onClick( Touch touch ) {
				DeviceCompat.openURI( "https://" + LNK_SHPX );
			}
		};
		add( shpxhotArea );

		Image wata = Icons.WATA.get();
		wata.x = wataOffset + (colWidth - wata.width()) / 2;
		wata.y = SPDSettings.landscape() ?
						colTop:
						shpxlink.top() + wata.height + 20;
		align(wata);
		add( wata );

		new Flare( 7, 64 ).color( 0x112233, true ).show( wata, 0 ).angularSpeed = +20;

		RenderedText wataTitle = renderText( TTL_WATA, 8 );
		wataTitle.hardlight(Window.TITLE_COLOR);
		add( wataTitle );

		wataTitle.x = wataOffset + (colWidth - wataTitle.width()) / 2;
		wataTitle.y = wata.y + wata.height + 11;
		align(wataTitle);

		RenderedTextMultiline wataText = renderMultiline( TXT_WATA, 8 );
		wataText.maxWidth((int)Math.min(colWidth, 120));
		add( wataText );

		wataText.setPos(wataOffset + (colWidth - wataText.width()) / 2, wataTitle.y + wataTitle.height() + 12);
		align(wataText);
		
		RenderedTextMultiline wataLink = renderMultiline( LNK_WATA, 8 );
		wataLink.maxWidth((int)Math.min(colWidth, 120));
		wataLink.hardlight(Window.TITLE_COLOR);
		add(wataLink);
		
		wataLink.setPos(wataOffset + (colWidth - wataLink.width()) / 2 , wataText.bottom() + 6);
		align(wataLink);
		
		TouchArea hotArea = new TouchArea( wataLink.left(), wataLink.top(), wataLink.width(), wataLink.height() ) {
			@Override
			protected void onClick( Touch touch ) {
				DeviceCompat.openURI( "https://" + LNK_WATA );
			}
		};
		add( hotArea );

		
		Archs archs = new Archs();
		archs.setSize( Camera.main.width, Camera.main.height );
		addToBack( archs );

		ExitButton btnExit = new ExitButton();
		btnExit.setPos( Camera.main.width - btnExit.width(), 0 );
		add( btnExit );

		fadeIn();
	}
	
	@Override
	protected void onBackPressed() {
		ChainedDungeon.switchNoFade(TitleScene.class);
	}
}
