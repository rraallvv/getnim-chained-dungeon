/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Shattered Pixel Dungeon
 * Copyright (C) 2014-2019 Evan Debenham
 *
 * Chained Dungeon
 * Copyright (C) 2019 Rhody Lugo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.teknologika.chaineddungeon.items.journal;

import com.teknologika.chaineddungeon.journal.Document;
import com.teknologika.chaineddungeon.messages.Messages;
import com.teknologika.chaineddungeon.sprites.ItemSpriteSheet;

public class AlchemyPage extends DocumentPage {
	
	{
		image = ItemSpriteSheet.ALCH_PAGE;
	}
	
	@Override
	public Document document() {
		return Document.ALCHEMY_GUIDE;
	}
	
	@Override
	public String desc() {
		return Messages.get(this, "desc", document().pageTitle(page()));
	}
}
