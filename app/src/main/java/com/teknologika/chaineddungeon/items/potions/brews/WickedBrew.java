/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Shattered Pixel Dungeon
 * Copyright (C) 2014-2019 Evan Debenham
 *
 * Chained Dungeon
 * Copyright (C) 2019 Rhody Lugo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.teknologika.chaineddungeon.items.potions.brews;

import com.teknologika.chaineddungeon.Assets;
import com.teknologika.chaineddungeon.Dungeon;
import com.teknologika.chaineddungeon.actors.blobs.Blob;
import com.teknologika.chaineddungeon.actors.blobs.ParalyticGas;
import com.teknologika.chaineddungeon.actors.blobs.ToxicGas;
import com.teknologika.chaineddungeon.items.potions.PotionOfParalyticGas;
import com.teknologika.chaineddungeon.items.potions.PotionOfToxicGas;
import com.teknologika.chaineddungeon.scenes.GameScene;
import com.teknologika.chaineddungeon.sprites.ItemSpriteSheet;
import com.watabou.noosa.audio.Sample;

public class WickedBrew extends Brew {
	
	{
		image = ItemSpriteSheet.BREW_WICKED;
	}
	
	@Override
	public void shatter(int cell) {
		if (Dungeon.level.heroFOV[cell]) {
			splash( cell );
			Sample.INSTANCE.play( Assets.SND_SHATTER );
		}
		GameScene.add( Blob.seed( cell, 1000, ToxicGas.class ) );
		GameScene.add( Blob.seed( cell, 1000, ParalyticGas.class ) );
	}
	
	@Override
	public int price() {
		//prices of ingredients
		return quantity * (30 + 40);
	}
	
	public static class Recipe extends com.teknologika.chaineddungeon.items.Recipe.SimpleRecipe {
		
		{
			inputs =  new Class[]{PotionOfToxicGas.class, PotionOfParalyticGas.class};
			inQuantity = new int[]{1, 1};
			
			cost = 1;
			
			output = WickedBrew.class;
			outQuantity = 1;
		}
		
	}
}
