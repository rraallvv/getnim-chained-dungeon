/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Shattered Pixel Dungeon
 * Copyright (C) 2014-2019 Evan Debenham
 *
 * Chained Dungeon
 * Copyright (C) 2019 Rhody Lugo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.teknologika.chaineddungeon.levels.traps;

import com.teknologika.chaineddungeon.Assets;
import com.teknologika.chaineddungeon.Dungeon;
import com.teknologika.chaineddungeon.actors.Actor;
import com.teknologika.chaineddungeon.actors.Char;
import com.teknologika.chaineddungeon.actors.hero.Hero;
import com.teknologika.chaineddungeon.actors.mobs.Mob;
import com.teknologika.chaineddungeon.effects.CellEmitter;
import com.teknologika.chaineddungeon.effects.Speck;
import com.teknologika.chaineddungeon.items.Heap;
import com.teknologika.chaineddungeon.items.Item;
import com.teknologika.chaineddungeon.items.scrolls.ScrollOfTeleportation;
import com.teknologika.chaineddungeon.messages.Messages;
import com.teknologika.chaineddungeon.scenes.GameScene;
import com.teknologika.chaineddungeon.utils.BArray;
import com.teknologika.chaineddungeon.utils.GLog;
import com.watabou.noosa.audio.Sample;

public class WarpingTrap extends Trap {

	{
		color = TEAL;
		shape = STARS;
	}

	@Override
	public void activate() {
		CellEmitter.get(pos).start(Speck.factory(Speck.LIGHT), 0.2f, 3);
		Sample.INSTANCE.play(Assets.SND_TELEPORT);
		
		Char ch = Actor.findChar(pos);
		if (ch != null && !ch.flying) {
			if (ch instanceof Hero) {
				ScrollOfTeleportation.teleportHero((Hero) ch);
				BArray.setFalse(Dungeon.level.visited);
				BArray.setFalse(Dungeon.level.mapped);
				GameScene.updateFog();
				Dungeon.observe();
				
			} else {
				int count = 10;
				int pos;
				do {
					pos = Dungeon.level.randomRespawnCell();
					if (count-- <= 0) {
						break;
					}
				} while (pos == -1);
				
				if (pos == -1 || Dungeon.bossLevel()) {
					
					GLog.w(Messages.get(ScrollOfTeleportation.class, "no_tele"));
					
				} else {
					
					ch.pos = pos;
					if (ch instanceof Mob && ((Mob) ch).state == ((Mob) ch).HUNTING) {
						((Mob) ch).state = ((Mob) ch).WANDERING;
					}
					ch.sprite.place(ch.pos);
					ch.sprite.visible = Dungeon.level.heroFOV[pos];
					
				}
			}
		}
		
		Heap heap = Dungeon.level.heaps.get(pos);
		
		if (heap != null){
			int cell = Dungeon.level.randomRespawnCell();
			
			Item item = heap.pickUp();
			
			if (cell != -1) {
				Dungeon.level.drop( item, cell );
			}
		}

	}
}
