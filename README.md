Chained Dungeon
=======================

A Roguelike RPG, with randomly generated levels, items, enemies, and traps!
Based on the source code of Pixel Dungeon, by Watabou.

Chained Dungeon uses gradle and is most easily compiled/edited using Android Studio.

Chained Dungeon on Google Play:
https://play.google.com/store/apps/details?id=com.teknologika.chaineddungeon

Source code of original Shattered Pixel Dungeon:
https://github.com/00-Evan/shattered-pixel-dungeon
